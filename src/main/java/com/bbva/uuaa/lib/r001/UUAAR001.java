package com.bbva.uuaa.lib.r001;

import java.util.List;

import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.dto.banco.PaginationInDTO;
import com.bbva.uuaa.dto.banco.PaginationOutDTO;

/**
 * The  interface UUAAR001 class...
 */
public interface UUAAR001 {

	//METODO DECLARADO PARA PODER SER UTILIZADO EN IMPLEMENTACION
	boolean executeCreate(CuentaDTO cuenta, CuentaDTO divisa, CuentaDTO tipoCuenta, CuentaDTO importe); 
	
	List<CuentaDTO> executeRead(Double cuenta, PaginationInDTO paginationIn);
	
	PaginationOutDTO executeGetPaginationDTO(Double cuenta, PaginationInDTO paginationIn);

}
